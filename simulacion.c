#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//Definir limite maximo
int LIMITE_INPUTS = 10;

//Definiendo las transiciones
int NO_CARROS = 0;		// = 00
int CARROS_ESTE = 1;	// = 01
int CARROS_NORTE = 2;	// = 10
int AMBOS_LADOS = 3;	// = 11

//Definiendo los estados
int goNORTE = 1;
int waitNORTE = 2;
int goESTE = 3;
int waitESTE = 4;

//Variable para el estado actual
int estado_actual;// = goNORTE;

int MaquinaEstados(int t, int estado_nuevo);
void msgSalida();

int main(){
	int contador = 0;
	int array_inputs[LIMITE_INPUTS];

	//Simulando entradas
	array_inputs[0] = NO_CARROS;
	array_inputs[1] = CARROS_ESTE;
	array_inputs[2] = CARROS_ESTE;
	array_inputs[3] = AMBOS_LADOS;
	array_inputs[4] = CARROS_NORTE;
	array_inputs[5] = CARROS_NORTE;
	array_inputs[6] = CARROS_NORTE;
	array_inputs[7] = AMBOS_LADOS;
	array_inputs[8] = CARROS_ESTE;
	array_inputs[9] = NO_CARROS;

	estado_actual = goNORTE;

	while(contador < LIMITE_INPUTS){
		estado_actual = MaquinaEstados(array_inputs[contador], estado_actual);
		msgSalida();
		contador++;
	}

	printf("\n");
	return 0;
}

int MaquinaEstados(int t, int estado_nuevo){
	if(estado_nuevo == goNORTE){
		if(t == NO_CARROS || t == CARROS_NORTE){
			estado_nuevo = goNORTE;
		}
		if(t == CARROS_ESTE || t == AMBOS_LADOS){
			estado_nuevo = waitNORTE;
		}
		sleep(3);
	}else if(estado_nuevo == waitNORTE){
		if(t == NO_CARROS || t == CARROS_ESTE || t == CARROS_NORTE || t == AMBOS_LADOS){
			estado_nuevo = goESTE;
		}
		sleep(2);
	}else if(estado_nuevo == goESTE){
		if(t == NO_CARROS || t == CARROS_ESTE){
			estado_nuevo = goESTE;
		}
		if (t == CARROS_NORTE || t == AMBOS_LADOS){
			estado_nuevo = waitESTE;
		}
		sleep(3);
	}else if(estado_nuevo == waitESTE){
		if(t == NO_CARROS || t == CARROS_ESTE || t == CARROS_NORTE || t == AMBOS_LADOS){
			estado_nuevo = goNORTE;
		}
		sleep(2);
	}
	return estado_nuevo;
}

void msgSalida(){
	if(estado_actual == goNORTE){
		printf("\n<------------------------------>");
		printf("\n  En el estado [goNORTE]");
		printf("\n  Salida de los Leds es [0x21]");
		printf("\n  [Led (RED) en (ESTE)(ON) y Led (GREEN) en (NORTE)(ON)]");
		printf("\n<------------------------------>");
	}else if(estado_actual == waitNORTE){
		printf("\n<------------------------------>");
		printf("\n  En el estado [waitNORTE]");
		printf("\n  Salida de los Leds es [0x22]");
		printf("\n  [Led (RED) en (ESTE)(ON) y Led (YELLOW) en (NORTE)(ON)]");
		printf("\n<------------------------------>");
	}else if(estado_actual == goESTE){
		printf("\n<------------------------------>");
		printf("\n  En el estado [goESTE]");
		printf("\n  Salida de los Leds es [0xc]");
		printf("\n  [Led (GREEN) en (ESTE)(ON) y Led (RED) en (NORTE)(ON)]");
		printf("\n<------------------------------>");
	}else if(estado_actual == waitESTE){
		printf("\n<------------------------------>");
		printf("\n  En el estado [waitESTE]");
		printf("\n  Salida de los Leds es [0x14]");
		printf("\n  [Led (YELLOW) en (ESTE)(ON) y Led (GREEN) en (NORTE)(ON)]");
		printf("\n<------------------------------>");
	}
}