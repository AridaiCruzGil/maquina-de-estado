// google test para semaforo (main.cpp)
#include "main.c"
#include <gtest/gtest.h>

TEST(Estado,stateMachine){
	ASSERT_EQ(goNORTE, MaquinaEstados(NO_CARROS, goNORTE));
	ASSERT_EQ(waitNORTE, MaquinaEstados(CARROS_ESTE, goNORTE));
	ASSERT_EQ(goESTE, MaquinaEstados(CARROS_ESTE, waitNORTE));
	ASSERT_EQ(waitESTE, MaquinaEstados(AMBOS_LADOS, goESTE));
	ASSERT_EQ(goNORTE, MaquinaEstados(CARROS_NORTE, waitESTE));
}

TEST(Estado0,stateMachine0){
	ASSERT_EQ(goNORTE, MaquinaEstados(CARROS_NORTE, goNORTE));
}

TEST(Estado1,stateMachine1){
	ASSERT_EQ(goNORTE, MaquinaEstados(CARROS_NORTE, goNORTE));
}

TEST(Estado2,stateMachine2){
	ASSERT_EQ(waitNORTE, MaquinaEstados(AMBOS_LADOS, goNORTE));
}

TEST(Estado3,stateMachine3){
	ASSERT_EQ(goESTE, MaquinaEstados(CARROS_ESTE, waitNORTE));
}

TEST(Transicion4,stateMachine4){
	ASSERT_EQ(goESTE, MaquinaEstados(NO_CARROS, goESTE));
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}